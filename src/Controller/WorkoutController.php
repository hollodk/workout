<?php

namespace App\Controller;

use App\Entity\Workout;
use App\Form\WorkoutType;
use App\Repository\WorkoutRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/workout')]
class WorkoutController extends AbstractController
{
    #[Route('/', name: 'workout_index', methods: ['GET'])]
    public function index(WorkoutRepository $workoutRepository): Response
    {
        $goal = 140000;
        $startWeight = 120;

        $current = $workoutRepository
            ->createQueryBuilder('w')
            ->select('SUM(w.calories)')
            ->getQuery()
            ->getSingleScalarResult()
        ;

        $workouts = $workoutRepository->findBy(
            [],
            ['id' => 'DESC']
        );

        $scoreboard = [];
        $now = new \DateTime();

        for ($i = 0; $i < 4; $i++) {
            $curr = clone $now;
            $key = $curr->format('Y, W');

            $title = sprintf('week %s, %s',
                $curr->format('W'),
                $curr->format('Y')
            );

            $scoreboard[$key] = [
                'date' => $curr,
                'title' => $title,
                'calories' => 0,
                'score' => 0,
                'duration' => 0,
            ];

            $now->modify('-1 week');
        }

        foreach ($workouts as $workout) {
            $key = $workout->getCreatedAt()->format('Y, W');

            if (isset($scoreboard[$key])) {
                $scoreboard[$key]['calories'] += $workout->getCalories();
                $scoreboard[$key]['score'] += $workout->getCalories()*100/$startWeight;
                $scoreboard[$key]['duration'] += $workout->getDuration();
            }
        }

        return $this->render('workout/index.html.twig', [
            'workouts' => $workouts,
            'goal' => $goal,
            'current' => $current,
            'start_weight' => $startWeight,
            'scoreboard' => $scoreboard,
        ]);
    }

    #[Route('/new', name: 'workout_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $workout = new Workout();
        $workout->setCreatedAt(new \DateTime());

        $form = $this->createForm(WorkoutType::class, $workout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($workout);
            $entityManager->flush();

            return $this->redirectToRoute('workout_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('workout/new.html.twig', [
            'workout' => $workout,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'workout_show', methods: ['GET'])]
    public function show(Workout $workout): Response
    {
        return $this->render('workout/show.html.twig', [
            'workout' => $workout,
        ]);
    }

    #[Route('/{id}/edit', name: 'workout_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Workout $workout, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(WorkoutType::class, $workout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('workout_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('workout/edit.html.twig', [
            'workout' => $workout,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'workout_delete', methods: ['POST'])]
    public function delete(Request $request, Workout $workout, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$workout->getId(), $request->request->get('_token'))) {
            $entityManager->remove($workout);
            $entityManager->flush();
        }

        return $this->redirectToRoute('workout_index', [], Response::HTTP_SEE_OTHER);
    }
}
