<?php

namespace App\Form;

use App\Entity\Workout;
use Psr\Container\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkoutType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $activities = [
            'AirBike' => 'air-bike',
            'Bike' => 'bike',
            'Hiking' => 'hiking',
            'Jumprope' => 'jumprope',
            'Run' => 'run',
            'Skiing' => 'skiing',
            'Walk' => 'walk',
            'Weight' => 'weight',
        ];

        $builder
            ->add('calories')
            ->add('duration', null, [
                'help' => 'Duration in minutes',
            ])
            ->add('activity', ChoiceType::class, [
                'choices' => $activities,
            ])
            ->add('description', null, [
                'attr' => [
                    'rows' => 10,
                ],
            ])
            ->add('createdAt')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Workout::class,
        ]);
    }
}
